# MP Select Mini Controller Board Fan Mount

## Description

This is a 3D-printable fan mount for the MP Select Mini 3D printer's controller board. It can be printed on the printer itself. It requires some additional hardware, and a soldered connection to 12V power on the controller board.

This mount was designed with Autodesk Fusion 360. I have included the
.f3d source file for use with Fusion 360, .stp files for use with most
3D solid modeling software, .stl files for import into slicing
utilities, and the .gcode file that I created with Cura to print this
on my own MP Select Mini 3D Printer.

## Parts Required

In addition to this mount, you will also need:

* 40mm 12VDC fan, such as the "Scythe Mini KAZE ULTRA 40mm Silent Mini Fan (SY124020L)" from Amazon.com, which conveniently includes screws suitable for mounting the fan onto the printed bracket.

* 3 each M3 x 16mm male-female standoffs, such as McMaster-Carr part number 93655A015.

## Pictures

Here is the controller board, visible after removing the bottom plate
of the printer enclosure:

![](pics/IMG_3748.jpg?raw=true)

Solder the fan's power wires to the 12VDC power input on the bottom of
the printed circuit board. I cut off one end of the adapter cable that
came with the fan, so I can still easily unplug the fan if needed.

![](pics/IMG_3754.jpg?raw=true)

Replace three of the mounting screws with the standoffs, then install
the mount on the standoffs using the screws you just removed. There
should be a lock washer underneath each screw; place the lock washers
between the standoffs and circuit board.

![](pics/IMG_3749.jpg?raw=true)
![](pics/IMG_3751.jpg?raw=true)

Install the fan on the mount with the screws that came with the fan:

![](pics/IMG_3753.jpg?raw=true)
![](pics/IMG_3752.jpg?raw=true)
![](pics/IMG_3755.jpg?raw=true)


## License

This design is open source and public domain. Use at your own risk of
delight.

